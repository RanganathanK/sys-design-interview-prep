1. Achieving high availability
    - Partitioning - ability to split the data in it across hosts, partition to follow a raid like system where you don't need all paritions at a time to restore the original entity
    - Replication - ability to replicate the data into regional hosts so if one is available
    - No single centralized server/process - this could result in single point failures
    - The avaibality should be applied to all components in the application, such as application server, database, storage structures so on
    - High available system mean, it can with stand infrafailures underneath, this is different from scalability
    - SSD's  or Disks, regional calls over network, CDN
    - Read/Write amplifications in SSD's
    - Keeping read/write servers in diff paths, without them overlapping, and avoid seek time

2. Ensure scalability with load stesting
    - Your systems is scalable if it maintains the latency for higher throughputs
    - Verify it using load and stress testing, achieved mainly with horizontal scaling

3. Why containers are better for horizontal scaling 
    - The problem is in auto-scaling, when throughput increases, we have to auto-scale our services, however the auto-scaling comes with a time cost, if you build on Ec2 or bare hosts, first instance has to be provisioned, then a virutual OS needs to be installed, then your application has to be loaded.
    - With containers the time taken to boot up a new service is significantly reduced, with hardware virtualisation you can spin up a container by loading only the necessary components, and avoid the OS installation, and you provision new instances only when you run out of systems, so the overall amortized time complexity is reduced

4. Provisioning scaling upfront to reduce cost and time 
    - Its often good idea to predict or calculate the upcoming traffic and provision enough instances upfront, this saves you cost, take AWS for example, reserved EC2 instances cost only 30% of On-demand instances, again, you don't have to wait for auto-scalling to provide new on-demand instance
    - Cons is if they calculation is not effective then you waste resources and cost
    - Validate this with load tests, and before we know we will get a spike provision high capacity to all services

5. Consistency levels of the application 
    - Provide consistency levels for each infra - example last write wins, system uses slopy quoram (number of distributed nodes a write happens before its marked successful)
    - Apply reads and writes in separate paths to DB, the writes happen through leader, and reads happen through follower, and when files are uploaded there is usually a processing and quoram involved
    - Data needs to be replicated in nodes, however for gettign it back parallel calls are poor ways, because too much of bandwidth, and you will be dicarding most calls
    - Using hinted hand off technique, to accept writes always, if a node is down, the next node can accept, and keep a metadata of it online, and then when node is up, a background process moves the data to actual node it was intended
    - Ordering events in distributed systems 
        - using Vector clocks - provides partial ordering, basically counters, more memory required
        - using CRDT's - example raik DB uses this, complex to build
        - using last write wins strategy, used by DDB and many Amazon services
    - How to count how many replica's are needed, its decided by consistency protocol (N, R, W) - N represents replcias, R- nodes to perform read, W - nodes to perform writes, DDB uses (3, 2, 2), 3 replices of each data, two nodes invovled in every read and every write
    - DDB or other systems after a write happens, the write is replicated to N-1 number of other nodes, however first 2 nodes the call is sync, and rest its async, and if any node is down, a hint off is written, and once node is up then its re-written using backgournd job - this behavior also have a window of vulnerability as the async may take its time and during which if a read happens, we may encounter old value
    - Every read is a state machine, where requests are sent to N nodes, and wait for R replices, and re-conline results, fix any conflicts send output, and wait for more time from other requests N - R, and fix if any of them lagging behind, this makes read heavy tough, use merkel trees for optimising this
    - Each node in the system have partition logic, replication logic, and coordination logic - coordinator is elected probably through paxos
    - Every replica matians a merkel tree, which is a binary tree of hashes over data ranges in the node, compare and update the part where hash mismatches
    - Coordinating a whole cluster involves O(N2) heartbeats, so gossip protocol is used, a node sends its heart beats only to next two nodes every seconds, and but this could cause un-reachable nodes, like nodes newly added, so seed nodes or boostrap nodes are elected and all communication pass through them - they are again elected using paxos, each node is capable of being an seed node, and system maintains minimum seed nodes for better availability
    - Accrual failure detection - instead of fully relying on heart beats for system up or down, this could have false positives, because network issues could cause heart beat failures despite not being up, so the algorithm, rather it keep suspicion levels for each nodes, as node's suspision levels increase 
    - In-sync replicas's are replica's that are insync with the leader, only these replicas are allowed to become leaders
    - Avoid single long write ahead log files, they are segmented and the file offset and file pointers are kept in memory, and also old files in that case can just be deleted instead of any random access
    - High water-mark offset is used to not send messages that are not available in other patitions in kafka, at any time high water mark is the min offset being replicated across partitions
    - Leting client decide replciation factor is one way allowing high througput
    - Use check points, for example GFS uses memory snapshots directly stored, and then gets it back after recorvery then it starts replaying operaiton log from now on, that way you don't have to replay all the operation logs
    - advisory locks is when locks are acquired don't necessarily restrict access, but logically it tells you the danger in accessing resources - with proper locks, the file will be released only when the system re-starts
    - use version or epoch sequence numbers, they help in identifying stale data, failure duing replication, that can be recovered back, the same concept used in vector clocks for logical ordering
    - using TTL - time to live is an important pattern, sometimes, a parimary node is assigned to do all reads or take all writes until TTL
    - distance calculations in distrbuted systems are usually Common ancestor or Xor distances, that way nodes, are replicated to father distances, or manytimes quoram is followed that is node address + 1,2,3 etc
    - Erasure coding vs RAID - both are mechanisms to recover files without having to maintain duplicates all the time
    - GFS and HDFS are not posix compliant, they cannot just mount, however use fuse driver, to do that
    - GFS and HDFS are good only for large files, as the block size is large, for small files, too many metadata will result in inefficiency 
    - Latency is not a big deal because these are mainly used in map-reduce or other batch processing, where availability and consistencies are important

6. Meanings of different terms
    - Distributed - runs on multiple machines with a coordination but independently
    - Decentralized - no single process or a host controlling everything
    - Scalable - either horizontally by adding new machines, or vertically but increaseing power of a machine
    - Highly available - can perform even if some parts in the system fail like a node goes down, part of data missing, network down time
    - Durable - provides consistency, all data written can be read
    - Tunable consistency - configure read levels, configure write levels, configure number of replicas so on, make it strong or eventual consistent

7. Gurantee's in streaming
    - Almost all gurantees are achived by consumers commiting to zoo-keeper, and zoo-keeper updates the offset for the consumer
    - At most once is when a zoo-keeper, updates offset as soon as a consumer picked the message, the consumer may fail without processign the message here
    - At lease once is when a consumer consumes a message and commits after message is processed - that way we know the consumer processed it
    - Exactly once - is hard and from the consumer side you need to maintain a transaction where in message processing, and offset update happesn in one transaction, in any crash, a roll back will take place, and off set will not be incremented, and do a re-read

8. Distributed counter - big table is example of this
    - If we use key value store, and keep multiple columns and each column holds a small udpate at nano-second latency
    - when read happens we update the overall count and reply back, and also fix the write
    - the same can be done for distributed throttling, where in read can repair the counts and discard older ones or use modulus to do that

9. Desiging a database be it SQL or NoSQL needs the following
    - How to read, upadte and delete the data 
    - How to perform reads sequentially
    - Data needs to stored in blocks otherwise disk seeks will slow down subsequent reads
    - Always write incoming records as append only sequences in a commit log - this will be a diaster recovery solution
    - Acquire lock on the tinest object as possible
    - Periodic compaction, either minor (write to memory then to disk), or major (read through blocks discard duplicates) or do snapshots, and save them periodically
    - versiong and timestamp of the data - is useful in no-sql stores for conflict resolution and optimistic locking
    - Separation between control and data, that is creating table, adding access lists, veirfying auth vs actually reading/writing data, a master coordinates the control flow, and the followers take care of actual read and write, with a client library, and all of them coordinated through a lock service like zoo keeper or chubhy

10. Bloom filters
    - Many storage and DB services can have a bloom filter layer to filter out reads that are not present, common in cases when user serarches for non-existant resources, particularly if you have to search in a disk for primary keys in GFS or Big Tables

11. Security of application
    - Provide support for Oauth and openId connect, and use AWS cognito which acts an authorizer

12. Picking right database, SQL or NoSQL or Columnar
    - Data size, this decides a lot, because each DB may have constraitns
    - Conflict tolerance in concurrent updates
    - Date shape and its access pattern - data shape between messaging is diff from time series, and the access pattern is different, OLAT vs OLTP
    - Data transactionality
    - Data read/write frequency
    - Data read sequentially vs random access - in genral sequntial read is faster due to OS paging and lesser disk seek time
    - Using SSD's for storage have write amplification problem
    - Eventual consistency - could be problematic, so use quoram for it.
    - Delete's aren't isntant in most cases, they are simply tombstones, and these are only removed during compation, while read the reader resolves them, so that way read is costlier than write, this happens in Dynamodb, Cassandra

13. Choice of languages and frameworks
    - Use of Go-routines instead of java threads to make multi-threading faster
    - Erlang frameworks better for messaging systems due to number of connections it can create
    - Flutter/React native for building one application across multiple platforms

14. Check pointing
    - Often times services stop, for example crawler can go down, scheduler can go down as a whole, and an LSE could be caused
    - At times, create check points, create snapshots periodically, and use it for recoving from any services
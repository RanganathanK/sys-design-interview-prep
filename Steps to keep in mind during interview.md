### Follow the below steps in system design

#### Design stages and behaviors 

1. Requirements collection & clarifications
2. Risks and trade offs
3. Detail design
4. Efficacy and communication

#### Key design considerations and risk areas

1. User experience
2. Scale and performance
3. Reliability and fault tolerance
4. Privacy and security
5. Simplicity and maintenance
6. Cost, time, versioning

#### TLDR version

1. Clarifications & Requirments analysis
2. Identify risks - Depedencies, LSE
3. User experience - API/frontend
4. Data life cycles - critical paths
5. Componentised design with trade offs
    - Distributed Compute/storage/DB design
    - Pre-processing/Offline components
    - System constraints
    - Reliablity, fault tolerance
6. Logging, Monitoring, Alarming
7. Security - encryption, privacy
8. Time & Cost estimates

#### My steps to handle a design problem

1. Disambiguate the problem - are you clear on requirements and assumptions
    - Understand the question correctly
    - Ask cross questions, get the functional and non-functional requirements
    - Find system whether system is sync or async, read/write heavy, compuete/IO heavy, UI/API required, pre-processing required like building prefix tree, or video encondings, or image compressions write it down, use it to define your choices
2. Use cases - have you solved the right use-case
    - Ask what use-cases we want to support, get the crux use case
    - Do we need calculations  ? - we can do it at the end of design, reason being starting and ending estimates vary
    - Define the goals, milestones and boundries of the problem, and mention this to interviewer
    - Always stick with de-coupled micro-service solution
3. Breaking down the problems into modules
    - Identify the components of the problem, map it to a generic solution available
    - Establish a data flow between the different components
    - Use containers to sovle the problem, use side-car containers to optimise diff work in diff places, advantage of containers is fast start, as there is no virtual machiene, but only micro containers
4. Choices & Trade offs for scaling
    - Bring in multiple choices possible in different components, like connectivity, storage, caching etc
    - Mention the examples you saw before, your learnigns from HLD and LLD of different systems
    - think of bloom filters, consistent hashing, hinted hand off, optimistic locking, zoo-keeper like locking service, checksums, merkel trees, min sketch count, erasure coding, elastic search, metric beats, and compression, logging, monitoring, alerting solution, CDN, failure over to other DC's
    - Mention what is your choice and why you pick such choice, for availability, consistency and partition tolerance
    - Talk about constraitns in designs, example lambda 15 mins timeout, API gateway 10k limit, sharding limits and ordering issues, file sizes in storage systems and give work around for it
5. End 
    - Don't forget security aspects, think of TLS traffic, and Oauth mechanisms for auth and auth, proxy servers to do that
    - Multi-tenancy should be noted as well
    - Prove that you have converged to a solution
    - Talk about improvements that can be made
        - Now do your estimations, and optimise
        - Optimising the communication - using proto buffer/ion etc
        - Optimising the network calls using compression
        - Use RPC in place of rest
        - Build client libraries to make things easier
        - Caches are done at multiple levels, client side(service workers), network side, server side
        - Etags for client side caching
    
At any stage, try to keep the interview interactive, keep driving the interview yourself and let your interviwer talk less

Note: In a system design interview there is no one answer fits all, you keep on going depth and breath of the process

## System design problems 

### Core computer science problems
1. Design a job scheduler
    - https://medium.com/walmartglobaltech/an-approach-to-designing-distributed-fault-tolerant-horizontally-scalable-event-scheduler-278c9c380637

2. Design logging, monitoring, and alarming service
Understanding ELK or Amazon cloudwatch can help
    - https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/working_with_metrics.html

3. Designing general search (Working of elastic search)
    - https://hub.packtpub.com/how-does-elasticsearch-work-tutorial/

4. Design peer to peer distributedd search system with no server/client rather everynode is server and client
    - https://www.slideshare.net/PriyabrataDash2/kademlia-introduction-94882642

5. Design a localisation framework
    -  https://www.baeldung.com/java-8-localization

6. Design CI/CD, mostly basics of how things work here

7. Design a notificatin service, how to send notifications to mobiles, or emails
    - https://www.digitalvidya.com/blog/push-notifications/
    - https://android.stackexchange.com/questions/43970/how-is-whatsapp-able-to-receive-messages-when-not-in-use

### Common application oriented problems

1. Design type ahead search
    - https://medium.com/@prefixyteam/how-we-built-prefixy-a-scalable-prefix-search-service-for-powering-autocomplete-c20f98e2eff1

2. Design uber backend
    - https://blog.maddevs.io/how-we-built-a-backend-system-for-uber-like-map-with-animated-cars-on-it-using-go-29d5dcd517a

3. Design a document to be edited by multiple people
    - Understand what is operational transformation

4. Design a ticket booking system (Irctc, library booking also fall in same category)

5. Design a notepad or word or any document creation with history of edits
    - https://codewords.recurse.com/issues/two/git-from-the-inside-out - understanding how git works may help

6. Design google search auto-correct feature
    - https://towardsdatascience.com/autocorrect-in-google-amazon-and-pinterest-and-how-to-write-your-own-one-6d23bc927c81

7. Design facebook news feed or twitter feed 

8. Design recommendations based on friends and other details

9. Design google maps, how to show navigation, how to represent places, and roads

10. Desing distributed counter
    - https://medium.com/@istanbul_techie/a-look-at-conflict-free-replicated-data-types-crdt-221a5f629e7e

## OOPS design problems (to add)


## API design
How to design good REST API's, and RPC operations
    - https://docs.microsoft.com/en-us/rest/api/azure/
How to handle async api's
How to have version support for API's
How to handle securities for API'same
How to paginate API's
    - https://developer.atlassian.com/server/confluence/pagination-in-the-rest-api/
    - https://phauer.com/2018/web-api-pagination-timestamp-id-continuation-token/


## DB Design
Practice design of SQL and no SQL designs
1. Learn how does inmemory database works 
    - https://medium.com/@denisanikin/what-an-in-memory-database-is-and-how-it-persists-data-efficiently-f43868cff4c1
2. MongoDB or Dynamodb for NO-sql, try create schemas, 1:1, 1:N, M:N relationships
    - https://docs.mongodb.com/manual/core/data-modeling-introduction/
3. How to make design for faster reads and faster rights, know about indexes, how global and local indexes work.

## OOps concepts to know
1. Generics (class generics, method generics, and member generics)
2. Single or multiple interfaces inheritaance
3. Inhertiance and composition with generics
4. When does inhertiance or composition become hindrance, are we creating explosion of classes ?
5. Understaing threading, and how to use Async and Future
6. How dependency injection works

## Design patterns to know, know its advantages and disadvantages, and also make it work with generics
1. Strategy
    - https://www.javaspecialists.eu/archive/Issue123-Strategy-Pattern-with-Generics.html
2. Decorator
3. Visitor
    - https://oncodedesign.com/the-visitor-pattern-a-better-implementation/
4. Command
5. State pattern - mostly disadvantages
6. Rule pattern - say you want to arrive at a final based on several rues, common in discout making processes

## Technologies to know
1. Basic working of message queues (Kafkaa, rabbitMq, aws sqs)
    - https://www.cloudamqp.com/blog/part1-rabbitmq-for-beginners-what-is-rabbitmq.html
    - https://medium.com/@ranjeetvimal/message-queues-10-reasons-to-use-message-queuing-1923277a2e7f
    - https://medium.com/@rinu.gour123/kafka-for-beginners-74ec101bc82d
2. Basic working of streaming (Kafka streams, aws kinesis streaming)
3. How authorisation works, what is JWT, what is TLS, SSH, and how two way encryption works
4. What is TCP handshake, how network connection works at high level, what is the latency involved
5. Types of caching, and use of load balancing 
    - http://horicky.blogspot.com/2010/10/scalable-system-design-patterns.html
    - https://stackoverflow.com/questions/10558465/memcached-vs-redis
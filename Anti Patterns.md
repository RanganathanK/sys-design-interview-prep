### A collection of anti-patterns basically bad engineering practices
1. Stored procedures
    - Stored procedures are business logic you can do within the database system, this is bad, always keep the business logic in application code, what if you want to change data storage in future ?
    - Similarly JSP's, in JSP tags usually people add business logic, ideally it should be just UI, what if we chagne from JSP to react in future ?
2. Too many if's and else's 
    - This will become a coding mess overtime, and avoid this with predicates, or rule design pattern
        - https://softwareengineering.stackexchange.com/questions/206816/clarification-of-avoid-if-else-advice - check all examples here
        - https://medium.com/swlh/stop-using-if-else-statements-f4d2323e6e4 - another example
3. Updating incoming variables in an object
    - Try to keep incoming values pure, and don't update them, keeps the readability better, debugging would be more logical that way
4. Alarms for each and every service in distruted micro-service
    - Having de-duplication and aggregation of alarms across dependent services is important
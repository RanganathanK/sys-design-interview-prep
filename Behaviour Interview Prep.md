# Some of my high level projects, their challenges and 

## Microsoft
1. **Bing lite**
    - Building client side javascript library for other teams to consume - reduced set of animations, almost 100+ hot answers started supporting lite experiences due to this
    - Built application for network trace routing - it helped us understand that user requests to bing reaches more hops before reaching the server, MS later partnered with Airtel, Vodafone to fix it
    - Server side knowledge graph walk through - optimal graph walk through to pick just the simple information from backend knowledge graph called Satori
    - Dynamic start up system that identifies if you are on a slow network - patented by Microsoft - Basically a used RTT(Round trip time), PLT(Page load time), IP address mapping to location, GPS data, and Connection signal (2g,3g,wifi,4g) to come up with a forumla to identify if user is on a slow network
        - Memory optimisation done using bloom filter - reduced 400 MB file to 20 MB size, and 24 hash functions
        - Next up optimised the hash functions by taking advantage, we are only dealing with numbers, and derived one hash function result from other by just left shifting the outputs 
        - hash functions use random number generators, and large prime numbers and multiply them to get collision less values, once one value is computed we used left shifts to compute hash values of other hash functions that way we reduced 24 hash function computing into one hash function computing
2. **Cortana reminders**
    -  Built the schema for remidners - one of the most intelligent reminder applications, supports several use cases like "get my reminders about medicines", "remind me to go to park every monday and tuesday in a week, for next 2 months"
    - Vocie integration and text integration for reminders - integrated with windows and speaker
    - Worked on cortana connected home app and speaker - Integration with sonos for controlling home  
3. **Bing Translation**
    - Completely modified Bing translator - Fixed the v1, v2, v3, v4 mess across the services
    - Added support for synonyms and word examples by building bi-lingual dictionaries - parsed existing dictionaries, books in public domain, and got sentences
    - Added support for speech to text translation 
        - Worked with a team diff from mine, did work on optimising the speech library for different operations instead of doing a giant library, made it compile time dependent, contributed to open source code, fixed their call back methods 
        - This library was included as part of platform code, and has later become a standard for voice search in Bing
    - Added transliteration support - Data scientists built model for these, I worked on integrating it, built all the backend api's with versioning
    - Built Async api's for longer translation situations, sends the status code back instead of actual response, client can later poll it
    - Added auto-complete while translating - this is different from auto-suggest for the search, parsed books built prefix hash data structure similar to trie
    - Built the app for android translator

## Amazon
1. **Last mile services** - all logistics for last mile delivery to be used by delviery service partner(DSP) who work for amazon as a company, this is different from Delivery associates
   - Service that supports vehicle management 
   - This is a basically collection of many services, I worked across, or built most of them
   - They involve a high throughput, tier-1 inspection service - everyday vehicles that are used for delivery are inspected twice 
   - They involve a low throughput, tier-2 service to handle vehicle add/update/modification 
   - It involves multiple 3p vehicle provider integration for ordering armada vehicles - multiple backround jobs are done and integrated here - they send files of the inventory that we update the inventory handling collisions - used rule engine but its quite big, so dropped plan
   - It involves having a user website - user can log on, check their vehicle data, update it, and view invoices 
   - It involves a payments part, where based on active usage of vehicle, we built a service that aggregates vehicle data based on usage and auto-mate it, allowing manual overrides in it to handle disputes
   - Improvising the parking experience, support for adding bills for parking off-site
  
2. **Universal Feature catalog**
    - Initial optimisation of private links saving us 8000$ every month - team used api-gateway grossly inefficient
    - Berliner project work - Across team, working on serveral async and sync services, support distributed throttling, auto-scaling, built on containers for better abstraction
    - Handles high throuput as high as 50k on US, 68K on EU, 22K on Jp, so on. Optimised as much as possible on logging, on auto-scaling, using async flow and back ups and check points in places
    - Involes backfilling data, - handling corruption in the backfill

## Tech challenges
1. Memory challenge in Bing lite - resulting in launch date moved over
2. Built auto-suggest using prefix-hash insted of trie based existing auto-suggest, as they both have diff context
3. Migrating to GIT for Microsoft, taken sessions on it, wrote a small tool that converts your code changes from one version control to git
4. Went above and beyond and solved the speech integration problem 
5. E2E building systems in Last mile, handling collisions in updates, - too many if's and else conditions, too many transformers, this was updated by me, keeping it simple
6. Challenges due to vin decoding, designed work on pipeline to dynamically update it, its basically a script that get trigged and runs as a batch job
7. High latency challenges, that I fixed, basically involved pagination, async api's not applicable, modifiied all queries, re-changed database, avoided the join like structure they already had 

## Situations 
1. **Co-worker interactions** 
    - Issue with designer, over androind and IOS designs for translation
2. **Co-worker feedback I have to give** 
    - Give an existing example of other doing same mistake and what we can do to learn and fix in future
    - First time mistake is always pardoned, mistakes repeated are taken very seriously 
3. **Best feedback I received** 
    - Don't stop your solution for lack of tech, snr framework didn't support node that time, so couldn't compile speech library, later I myself fixed it and shipped 18 features
    - Don't complain about existing bad code, it could be multiple reasons for that, and since you are here, you can fix it.
4. **Best time to stop working on something** - tough decision, I stopped the uber integration idea, didn't accept multiple answers in bing because lite framework support wasn't there
5. **Work done beyond responsibilities** 
    - Fixed the node-js library for speech in Microsoft translator, made it compile time consumable, worked on fixing the console log issues by implementing call back
    - Worked with linked in to ramp up the lite solution
6. **Praising feedback I received**
    - Git migration and wrote a tool to help move code from source depot(version control) to git, and took a learning session for folks
    - I worked on connected home, that was later transitioned to other team, and I helped them out, worked with them over a week to fix issues, live site first is the motto
7. **Handling a co-worker in efficiency** 
    - Satya nadella recommends say it to face to feel the heat, but you can explain seriouness of issues, give previous examples of handling situations, one time an intern checked in hardcoded map values, I have to meet with him and his mentor for further fixes
8. **Handling incidents gracelly**, 
    - Taking ownership, Cortana speaker failures for view files, did static analysis fixed it in future, its important issues are fixed in future
9. **Big Strength** 
    - I dive deep and take ownerhsip on the work that I do, always think beyond just current but future changes
13. **Weakness** 
    - Got few feedbacks that I do give agressive estimates, didn't estimate time for testing, and Operational work, and pro-actively communicate the risks, at one time, my manager ads 1 week on top of my estimates when I send it to other teams
14. **Influenced a decision** 
    - Deprecate support from middle mile and moved to last mile database, by one time upgrading, did a POC showed it won't take much effort or time
    - Using the right schema for inspection and defects tables and optimise dynamoc db usage
    - Using Hex, and private links in new team, similarly KCL vs Lambda in UIFC team
# Questions on design interview

From my understanding there are several types of design questions that can be asked, I classifed them into groups below

## Data structure design
Example, let say we have a twitter like system, and we want to find top k hashtags in past one minute, one hour and one day

Now, this is a typical DS design question, where we have to come up with a combination of a complex data structure like for this one we need to build a hash indexed doubly linked list and maintain in sorted order to identify top k elements, etc. 

## Class design
Example, design a library for a job scheduler, or design an ATM machine, or design schema for ticket reservation

Here we have to come up with, class diagrams, proper inheritice and composition, to attain maximum object orientation so on

Follow educative-io Class design questions for more details


## Distributed system design
Here your design should capture the API/frontend, handle reliability, scallability, maintainability, multi-tenancy caching, storage choices, read/write, latecncy and performance issues, monitoring and alerting

1. Design tiny url system
2. Design paste bin 
3. Design collaborative document editor - google docs
4. Design uber backend
5. Design drop box
6. Design a youtube/netflix clone
7. Design google maps system
8. Design localisation framework
9. Design a csv to json converter

## Infrastructure design
Some example questions

1. Design a key value object store - s3 storage
2. Design in-memory database - like dynamoDb, mongoDB
3. Design an event bus - like SQS 
4. Design an logging, monitoring, and alerting sytem - like cloudwatch, time series DB
5. Design a distributed scheduler/cron
6. Design a distributed throttler
7. Design open search sytem
